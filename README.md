# JS Option Tutorial

Some sample code to introduce `Option`, a higher order type. 

A [higher order type](https://en.wikipedia.org/wiki/Type_constructor) (sometimes called a type constructor) is a type that given a type, gives you a new type. A common example is `List<_>`. If we give it the type `string`, we get the type `List<string>`. `Option` is a similar higher order type, they have the same [*kind*](https://en.wikipedia.org/wiki/Kind_(type_theory)), which describes them both as `* -> *`. The first `*` is the type you provide and the second `*` is the new type that's returned.

`Option` represents a single element of a given type or none. For a variable of type `Option<string>`, valid instances are `Some("foo")` or `None`.

`Option`, like `List`, has helpful functions to work with its contained value. Apart from stronger typing there are other benefits to using it: safe application of business logic, composition and early termination.

You can run the sample code using the following. If you make changes and save, it should rerun automatically.
```
yarn install ; yarn start
```
Everything is in `src/index.js`, enjoy :)
