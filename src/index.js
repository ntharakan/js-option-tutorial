import { Option, Some, None } from 'funfix-core';

/*
 * Safe function application.
 */

 // Optional string value of type Option<string>.
let str =  Some("foo");

// Let's apply some logic to it, print.
str.map(console.log);

// If we don't have a string, we can still apply our logic safely, it simply won't execute.
str = None;
str.map(console.log); // No exceptions!

// We can also have default values.
console.log(str.getOrElse("bar"));

// Give it a try, you can use Option.of to create an option from given value.
const test = Option.of("foo"); // Try replacing "foo" with null or undefined to see what happens.
test.map(console.log);

/*
 * Composition
 */

 // We can apply logic over multiple optional values safely too.
 // Try changing one of the following to None.
const greeting = Some("hi");
const name = Some("nithin");

Option.map2(greeting, name, (g, n) => console.log(g + ' ' + n));
// The above works with functions that return optional values too, so you can chain functions with early termination :)
